<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201209201243 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE movie_artist (movie_id INT NOT NULL, artist_id INT NOT NULL, INDEX IDX_F89E0BF28F93B6FC (movie_id), INDEX IDX_F89E0BF2B7970CF8 (artist_id), PRIMARY KEY(movie_id, artist_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE movie_artist ADD CONSTRAINT FK_F89E0BF28F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_artist ADD CONSTRAINT FK_F89E0BF2B7970CF8 FOREIGN KEY (artist_id) REFERENCES artist (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE artist_movie');
        $this->addSql('ALTER TABLE movie DROP `release`');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE artist_movie (artist_id INT NOT NULL, movie_id INT NOT NULL, INDEX IDX_7D52C23C8F93B6FC (movie_id), INDEX IDX_7D52C23CB7970CF8 (artist_id), PRIMARY KEY(artist_id, movie_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE artist_movie ADD CONSTRAINT FK_7D52C23C8F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE artist_movie ADD CONSTRAINT FK_7D52C23CB7970CF8 FOREIGN KEY (artist_id) REFERENCES artist (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE movie_artist');
        $this->addSql('ALTER TABLE movie ADD `release` DATE NOT NULL');
    }
}
