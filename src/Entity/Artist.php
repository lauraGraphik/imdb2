<?php

namespace App\Entity;

use App\Repository\ArtistRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass=ArtistRepository::class)
 * @Vich\Uploadable
 */
class Artist
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @Vich\UploadableField(mapping="artists", fileNameProperty="portrait")
     */
    private $portraitFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $portrait;

    /**
     * @ORM\ManyToMany(targetEntity=Movie::class, mappedBy="actors")
     */
    private $hasPlayIn;

    /**
     * @ORM\OneToMany(targetEntity=Movie::class, mappedBy="director", orphanRemoval=true)
     */
    private $hasDirected;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->hasPlayIn = new ArrayCollection();
        $this->hasDirected = new ArrayCollection();
    }

    public function __toString(){
        return $this->getFirstname()." ".$this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getPortrait(): ?string
    {
        return $this->portrait;
    }

    public function setPortrait(?string $portrait): self
    {
        $this->portrait = $portrait;

        return $this;
    }

    /**
     * @return Collection|Movie[]
     */
    public function getActorMovies(): Collection
    {
        return $this->hasPlayIn;
    }

    public function addActorMovie(Movie $hasPlayIn): self
    {
        if (!$this->hasPlayIn->contains($hasPlayIn)) {
            $this->hasPlayIn[] = $hasPlayIn;
        }
        return $this;
    }

    public function removeActorMovie(Movie $hasPlayIn): self
    {
        $this->hasPlayIn->removeElement($hasPlayIn);

        return $this;
    }

    /**
     * @return Collection|Movie[]
     */
    public function getHasDirected(): Collection
    {
        return $this->hasDirected;
    }

    public function addHasDirected(Movie $hasDirected): self
    {
        if (!$this->hasDirected->contains($hasDirected)) {
            $this->hasDirected[] = $hasDirected;
            $hasDirected->setDirector($this);
        }

        return $this;
    }

    public function removeHasDirected(Movie $hasDirected): self
    {
        if ($this->hasDirected->removeElement($hasDirected)) {
            // set the owning side to null (unless already changed)
            if ($hasDirected->getDirector() === $this) {
                $hasDirected->setDirector(null);
            }
        }

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
 
    public function setPortraitFile(?File $portraitFile=null): void    
    {
        $this->portraitFile=$portraitFile;
        if (null!==$portraitFile) {
            $this->updatedAt=new\DateTimeImmutable();
        }
    }

    public function getPortraitFile(){
        return $this->portraitFile;
    }
}
