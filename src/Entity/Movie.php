<?php

namespace App\Entity;

use App\Repository\MovieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass=MovieRepository::class)
 * @Vich\Uploadable
 */
class Movie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @Vich\UploadableField(mapping="posters", fileNameProperty="poster")
     */
    private $posterFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $poster;


    /**
     * @ORM\ManyToMany(targetEntity=Artist::class, inversedBy="hasPlayIn")
     */
    private $actors;

    /**
     * @ORM\ManyToOne(targetEntity=Artist::class, inversedBy="hasDirected")
     * @ORM\JoinColumn(nullable=false)
     */
    private $director;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->actors = new ArrayCollection();
    }

    public function __toString(){
        return $this->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPoster(): ?string
    {
        return $this->poster;
    }

    public function setPoster(?string $poster): self
    {
        $this->poster = $poster;

        return $this;
    }

    /**
     * @return Collection|Artist[]
     */
    public function getActors(): Collection
    {
        return $this->actors;
    }

    public function addActor(Artist $actor): self
    {
        if (!$this->actors->contains($actor)) {
            $this->actors[] = $actor;
            $actor->addActorMovie($this);
        }

        return $this;
    }

    public function removeActor(Artist $actor): self
    {
        if ($this->actors->removeElement($actor)) {
            $actor->removeActorMovie($this);
        }

        return $this;
    }

    public function getDirector(): ?Artist
    {
        return $this->director;
    }

    public function setDirector(?Artist $director): self
    {
        $this->director = $director;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
    /**    
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile | null $imageFile     
     * */
    public function setPosterFile(?File $posterFile=null): void    
    {
        $this->posterFile=$posterFile;
        if (null!==$posterFile) {
            $this->updatedAt=new\DateTimeImmutable();
        }
    }

    public function getPosterFile(){
        return $this->posterFile;
    }
}
