<?php

namespace App\Form;

use App\Entity\Movie;
use App\Entity\Artist;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MovieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description')
            ->add('posterFile', VichImageType::class,[
                'required' => false,
                'allow_delete' => true,
                'delete_label' => "Supprimer l'affiche",
                'asset_helper' => true,
            ])
            ->add('actors', EntityType::class, [
                "class"=>Artist::class,
                "choice_label"=>"name",
                "multiple"=>true,
                "expanded"=>true
            ])
            ->add('director')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Movie::class,
        ]);
    }
}
