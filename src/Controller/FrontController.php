<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\MovieRepository;
use App\Entity\Movie;
use App\Entity\Artist;

class FrontController extends AbstractController
{
    /**
     * @Route("/", name="index_front")
     */
    public function index( MovieRepository $movieRepo): Response
    {
        return $this->render('front/index.html.twig', [
            'movies' => $movieRepo->findAll(),
        ]);
    }
    
    /**
     * @Route("/film/{id}", name="movie_front")
     */
    public function movie( Movie $movie): Response
    {
        return $this->render('front/movie.html.twig', [
            'movie' => $movie,
        ]);
    }

    /**
     * @Route("/artiste/{id}", name="artist_front")
     */
    public function artist( Artist $artist): Response
    {
        return $this->render('front/artist.html.twig', [
            'artist' => $artist,
        ]);
    }
}
